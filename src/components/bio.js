import React from "react"
import { StaticQuery, graphql } from "gatsby"

const Bio = () => (
  <StaticQuery
    query={graphql`
      query Bio {
        site {
          siteMetadata {
            title
            social {
              twitter
            }
          }
        }
      }
    `}
    render={data => (
      <>
        <h1>{data.site.siteMetadata.title}</h1>
        <h2>twitter: {data.site.siteMetadata.twitter} </h2>
      </>
    )}
  />
)

export default Bio
