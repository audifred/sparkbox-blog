import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"

const BigTitle = styled.h1`
  font-size: 2.75rem;
  margin-top: 0;
`
const Header = ({ location }) => {
  const rootPath = `${__PATH_PREFIX__}`
  let header
  const title = "Sparkbox Blog"

  return <header>{header}</header>
}

export default Header
