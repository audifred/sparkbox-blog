import React from "react"
import styled from "styled-components"
import Bio from "../components/bio"
import Header from "../components/header"

const Container = styled.div`
  margin: 3rem auto;
  max-width: 700px;
  width: 90vw;
`

const Layout = ({ children, location }) => (
  <Container>
    <Header location={location} />
    <Bio />
    {children}
  </Container>
)

export default Layout
