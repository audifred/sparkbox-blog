module.exports = {
  siteMetadata: {
    title: "Sparkbox Blog FSA 2020",
    siteUrl: "https://localhost:8000",
    description:
      "A space to learn about my apprenticeship experience @ Sparkbox",
    social: {
      twitter: "audifred_",
    },
  },
  plugins: ["gatsby-plugin-styled-components"],
}
